/* GNU Mailutils -- a suite of utilities for electronic mail
   Copyright (C) 2011-2025 Free Software Foundation, Inc.

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 3 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General
   Public License along with this library.  If not, see 
   <http://www.gnu.org/licenses/>. */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <mailutils/sockaddr.h>
#include <mailutils/errno.h>

int
mu_sockaddr_get_port (struct mu_sockaddr *sa, int *port)
{
  if (sa && port)
    {
      if (sa->addr->sa_family == AF_INET)
	{
	  struct sockaddr_in *s_in = (struct sockaddr_in *)sa->addr;
	  *port = ntohs (s_in->sin_port);
	  return 0;
	}
      else if (sa->addr->sa_family == AF_INET6)
	{
	  struct sockaddr_in6 *s_in6 = (struct sockaddr_in6 *)sa->addr;
	  *port = ntohs (s_in6->sin6_port);
	  return 0;
	}
    }
  return EINVAL;
}
